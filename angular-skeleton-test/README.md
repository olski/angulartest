Welcome to Angular skeleton repository!

# AngularSkeletonTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).






*********************************************************************************
*********************************************************************************

## Below help with the project structure:

*********************************************************************************
*********************************************************************************

~/media    

The media directory is used to store supporting files for the application. Things like requiremenets docs, text outlines, etc. This is the junk drawer for the project.

===============================================

~/src/app/core	

This module is for classes used by app.module. Resources which are always loaded such as route guards, HTTP interceptors, and application level services, such as the ThemeService and logging belong in this directory.

===============================================

~/src/app/data
  /schema
  /service

The data module is a top level directory and holds the schema (models/entities) and services (repositories) for data consumed by the application.

The schema directory holds the class definition files for data structures.

The service directory holds the services for fetching data. The service files are not necessarily a 1:1 match with schema files. 

===============================================

If your application consumes data from more than one source then the data directory should be restructured to contain subdirectories for each data source. Do not create multiple modules for each data source:

~/src/app/data
  /data-source-one
    /schema
    /service
  /data-source-two
    /schema
    /service
  /data.module.ts
  
===============================================

~/src/app/layout

The layout directory is a container of components which are declared in the AppModule. The directory contains page-level components of content such as a common footer, navigation, and header. It also contains page layouts for the different sections of your application.

===============================================

~/src/app/modules

The modules directory contains a collection of modules which are each independent of each other. This allows Angular to load only the module it requires to display the request thereby saving bandwidth and speeding the entire application.

In order to accomplish this each module must have its own routing which is a loadChildren route resource defined in the AppRoutingModule

For each new module run: ng generate module modules/NewModule

===============================================

~/src/app/shared

The shared module contains classes and resources which are used in more than one dynamically loaded module. By always loading with the application the shared components are ready whenever a module requests them.

The shared module is a good place to import and export the FormsModule and the ReactiveFormsModule. It is also good for the FontAwesomeModule and any other resource used by some modules some of the time but not all modules all of the time.

===============================================

~/src/styles

The directory is used to store scss style sheets for the application. It can contain themes, Bootstrap, Angular Material, and any other styles.

===============================================

The [+] indicates that the folder has additional files.

|-- app
     |-- modules
       |-- home
           |-- [+] components
           |-- [+] pages
           |-- home-routing.module.ts
           |-- home.module.ts
     |-- core
       |-- [+] authentication
       |-- [+] footer
       |-- [+] guards
       |-- [+] http
       |-- [+] interceptors
       |-- [+] mocks
       |-- [+] services
       |-- [+] header
       |-- core.module.ts
       |-- ensureModuleLoadedOnceGuard.ts
       |-- logger.service.ts
     |
     |-- shared
          |-- [+] components
          |-- [+] directives
          |-- [+] pipes
          |-- [+] models
     |
     |-- [+] configs
|-- assets
     |-- scss
          |-- [+] partials
          |-- _base.scss
          |-- styles.scss
          
          
          
How to make such a skeleton? 

ng new
mkdir media
ng generate module Core
ng generate module Shared
ng generate module Data
mkdir src/app/layout
mkdir src/app/modules
mkdir src/styles && mkdir src/styles/themes
json --version || npm install -g json
json -f tsconfig.json -I -c "this.compilerOptions.paths = {}"
json -f tsconfig.json -I -e "this.compilerOptions.paths['@app/*'] = ['app/core/*']" \
  -e "this.compilerOptions.paths['@shared/*'] = ['app/shared/*']" \
  -e "this.compilerOptions.paths['@env/*'] = ['environment/*']"